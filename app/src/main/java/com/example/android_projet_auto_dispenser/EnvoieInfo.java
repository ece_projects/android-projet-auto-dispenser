package com.example.android_projet_auto_dispenser;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

public class EnvoieInfo extends AsyncTask<String, Integer, String> {

    protected String doInBackground(String... token) {
        URL urls = null;
        String toreturn=null;
        HashMap<String,Double> values = new HashMap<>();
        Log.d("INFO", "le token dans async =" + token[0]);
        try {
            urls = new URL("https://api.superveezer.com/distributors/"+token[1]+"/usages");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) urls.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            con.setRequestMethod("PATCH");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        //con.setRequestProperty("X-HTTP-Method-Override", "PATCH");

        con.setRequestProperty("Authorization", "Bearer "+token[0]);
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        String jsonInputString = "{\r\n    \"numberOfUsages\":"+token[2]+",\r\n    \"batteryLevel\":"+token[3]+"\r\n}";

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            toreturn = response.toString();
            Log.d("info", toreturn);
            System.out.println(response.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return toreturn;
    }


    protected void onProgressUpdate(Integer... progress) {
    }

    protected void onPostExecute(Long result) {
    }
}
