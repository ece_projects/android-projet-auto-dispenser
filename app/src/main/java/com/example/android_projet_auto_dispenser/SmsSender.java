package com.example.android_projet_auto_dispenser;

import android.Manifest;
import android.content.pm.PackageManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import org.json.JSONException;
import org.json.JSONObject;

public class SmsSender {

    private String phoneNumber;
    private String message;

    public void sendSms(String number, String text){
        this.phoneNumber = number;
        this.message=text;
        this.sendSMS_by_smsManager();
    }

    private void sendSMS_by_smsManager()  {

        try {
            // Get the default instance of the SmsManager
            SmsManager smsManager = SmsManager.getDefault();
            // Send Message
            smsManager.sendTextMessage(phoneNumber,
                    null,
                    message,
                    null,
                    null);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
