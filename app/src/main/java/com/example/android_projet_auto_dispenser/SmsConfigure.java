package com.example.android_projet_auto_dispenser;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SmsConfigure extends Activity {

    private Button sendButton;
    private EditText voltageEdit, timePressedEdit;
    private AutoCompleteTextView phoneNumberEdit;
    private View.OnClickListener buttonListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configure_sms);
        Log.d("Dispenser","sms configure created");

        sendButton = (Button) findViewById(R.id.send_button);
        voltageEdit = (EditText) findViewById(R.id.voltage_editText);
        timePressedEdit = (EditText) findViewById(R.id.time_pressed_editText);
        phoneNumberEdit = (AutoCompleteTextView) findViewById(R.id.phone_number_editText);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, NUMBERS);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.phone_number_editText);
        textView.setAdapter(adapter);

        buttonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Dispenser","SMS SEND");
                JSONObject tosend = new JSONObject();
                try {
                    tosend.put("Tm",timePressedEdit.getText().toString());
                    tosend.put("V",voltageEdit.getText().toString());
                    new SmsSender().sendSms(phoneNumberEdit.getText().toString(),tosend.toString());
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        sendButton.setOnClickListener(buttonListener);

    }

    private static final String[] NUMBERS = new String[] {
            "0602508992", "0635613222", "0777960839", "0671748259"
    };



}
