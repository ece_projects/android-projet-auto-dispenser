package com.example.android_projet_auto_dispenser;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ImageButton optionsMenu;
    private Spinner deviceSelect;
    private ProgressBar batteryProgress,gelProgress;
    private TextView batteryLevelText, gelLevelText;
    private HashMap<String,String> bornes_asso = new HashMap<>();
    private SMSReceiver listener;
    private String data343="{\"batLevel\":0,\"gelLevel\":0}",data344="{\"batLevel\":0,\"gelLevel\":0}",data345="{\"batLevel\":0,\"gelLevel\":0}",data346="{\"batLevel\":0,\"gelLevel\":0}";
    private String nballowed="+33777960839;+33635613222;+33671748259;+33602508992";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        optionsMenu = (ImageButton) findViewById(R.id.menu);
        deviceSelect = (Spinner) findViewById(R.id.device_choser);
        batteryLevelText = (TextView) findViewById(R.id.battery_level_text);
        gelLevelText = (TextView) findViewById(R.id.gel_level_text);
        batteryProgress = (ProgressBar) findViewById(R.id.battery_level_progress);
        gelProgress = (ProgressBar) findViewById(R.id.gel_level_progress);
        deviceSelect = (Spinner) findViewById(R.id.device_choser);

        this.fill_bornes();
        this.createSMSlistener();

        deviceSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (deviceSelect.getSelectedItem().toString()){
                    case "344":
                        try {
                            editCircles(data344);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "345":
                        try {
                            editCircles(data345);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "343":
                        try {
                            editCircles(data343);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "346":
                        try {
                            editCircles(data346);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    @Override
    protected void onStart(){
        super.onStart();
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    public void showMenu(View view) {
        //Log.d("Dispenser","button clicked");
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return selectItem(item);
            }
        });

        popup.inflate(R.menu.options_menu);
        popup.show();
    }

    public boolean selectItem(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sms_config:
                Log.d("Dispenser","option sms configure");
                Intent intent = new Intent(this, SmsConfigure.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sms_config:
                Log.d("Dispenser","option sms configure");
                Intent intent = new Intent(this, SmsConfigure.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fill_bornes(){
        bornes_asso.put("+33777960839","343");
        bornes_asso.put("+33635613222","344");
        bornes_asso.put("+33671748259","345");
        bornes_asso.put("+33602508992","346");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, new String[] {
                "343", "344", "345", "346"
        });
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSelect.setAdapter(adapter);
    }

    private void createSMSlistener(){
        IntentFilter receiveFilter = new IntentFilter();
        receiveFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        receiveFilter.setPriority(100);

        listener = new SMSReceiver();
        registerReceiver(listener, receiveFilter);
    }

    private void make_request(ArrayList<String> recup) throws ExecutionException, InterruptedException, JSONException {

        String auth_token=null;
        try {
            auth_token = new getAuthToken().execute().get();
            Log.d("INFO", "le token est " + auth_token);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("INFO", "le token 2 est " + auth_token);
        String res = new EnvoieInfo().execute(auth_token,bornes_asso.get(recup.get(0)),recup.get(1),recup.get(2)).get();

        switch (bornes_asso.get(recup.get(0))){
            case "344":
                data344=res;
                deviceSelect.setSelection(1);
                break;
            case "345":
                data345=res;
                deviceSelect.setSelection(2);
                break;
            case "343":
                data343=res;
                deviceSelect.setSelection(0);
                break;
            case "346":
                data346=res;
                deviceSelect.setSelection(3);
                break;
        }

        editCircles(res);

        Toast.makeText(this,"MAJ "+ bornes_asso.get(recup.get(0))+" reçue !",Toast.LENGTH_LONG).show();


        Log.i("Application created", "True");
    }

    private void editCircles(String toset) throws JSONException {

        JSONObject json = new JSONObject(toset);
        int battery = (int)Float.parseFloat(json.get("batLevel").toString());
        int gel = (int)Float.parseFloat(json.get("gelLevel").toString());
        batteryProgress.setProgress(battery);
        batteryLevelText.setText(String.valueOf(battery)+"%");
        gelProgress.setProgress(gel);
        gelLevelText.setText(String.valueOf(gel)+"%");
    }

    public class SMSReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.i("Enter in onReceived", "True");
            Bundle bundle = intent.getExtras();
            Object[] pdus = (Object[]) bundle.get("pdus"); // Retrieve SMS messages
            SmsMessage[] messages = new SmsMessage[pdus.length];
            for (int i = 0; i < messages.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            String address = messages[0].getOriginatingAddress();
            if (nballowed.contains(address)) {
                String fullMessage = "";
                for (SmsMessage message : messages) {
                    fullMessage += message.getMessageBody();
                }

                ArrayList<String> tosend = new ArrayList<>();
                tosend.add(address);
                try {
                    JSONObject toparse = new JSONObject(fullMessage);
                    tosend.add(toparse.getString("Tm"));
                    tosend.add(toparse.getString("V"));
                    make_request(tosend);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                abortBroadcast();

            }
        }
    }

    @Override
    protected void onDestroy() { //Log de destruction de l'application
        super.onDestroy();
        unregisterReceiver(listener);
        Log.i("Application destroy", "True");
    }

    @Override
    protected void onResume() { //Log de reprise de l'application
        super.onResume();
        Log.d("Application resume", "True");
    }

    @Override
    protected void onStop() { //Log de stop
        super.onStop();
        Log.d("Application stoped", "True");
    }

}